import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class CoordinatorService {

    readonly baseURL = 'https://ab-recruit-menu.azurewebsites.net/api/GetMenuStructure?role=1';

    constructor(
        private http: HttpClient
    ) {}

    async getCoordinators(): Promise<any> {
        return await this.http.get(this.baseURL).toPromise();
    }
}
