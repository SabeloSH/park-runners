import { Component, OnInit } from '@angular/core';
import { CoordinatorService } from './coordinator.service';

@Component({
  selector: 'app-coordinator',
  templateUrl: './coordinator.component.html',
  styleUrls: ['./coordinator.component.scss']
})
export class CoordinatorComponent implements OnInit {
  dashboard: boolean;

  dashboardData = [];
  eventsData = [];
  data = [];
  events = false;
  isLoading = true;

  constructor(private coordinatorService: CoordinatorService) {
    // this.dashboard.
   }

  ngOnInit() {
    this.dashboard = true;
    this.coordinatorService.getCoordinators().then((response) => {
      this.isLoading = false;
      this.data = response.data.items;

      for (const key in response.data.items) {
        if (response.data.items[key].label === 'Dashboard') {
          this.dashboardData.push(response.data.items[key].label);
        }
      }

    }).catch((error) => {
      this.isLoading = false;
      console.log(error);
    });
  }

  viewEvents() {
    this.events = true;
    this.dashboard = false;
    this.eventsData = [];
    console.log(this.data);
    for (const key in this.data) {
      if (this.data[key].label === 'Events') {
        this.eventsData.push(this.data[key].label);
      }
    }
  }

  viewDashboard() {
    this.events = false;
    this.dashboard = true;
    this.dashboardData = [];
    for (const key in this.data) {
      if (this.data[key].label === 'Dashboard') {
        this.dashboardData.push(this.data[key].label);
      }
    }
  }

}
