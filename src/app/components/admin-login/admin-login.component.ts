import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.scss']
})
export class AdminLoginComponent implements OnInit {
  username: string;
  password: string;
  incorrect: boolean;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  login() {
    if (this.username === 'admin' && this.password === '123456') {
      this.router.navigate(['/coordinator']);
    } else {
      this.incorrect = true;
    }
  }
}
