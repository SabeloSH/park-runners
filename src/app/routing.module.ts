import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AdminLoginComponent } from './components/admin-login/admin-login.component';
import { CoordinatorComponent } from './components/coordinator/coordinator.component';

export const routes: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'admin-login', component: AdminLoginComponent },
    { path: 'coordinator', component: CoordinatorComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true })],
    exports: [RouterModule],
    providers: []
})
export class AppRoutingModule { }
